package no.ntnu.idagh.patientregister;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    public static PatientRegister patientRegister;

    @Test
    void addPatientTrue() {
        patientRegister = new PatientRegister();
        assertTrue(patientRegister.addPatient(new Patient("Vilde", "G", "Heidi","13515")));
    }
    @Test
    void addPatientFalse() {
        patientRegister = new PatientRegister();
        Patient p1 = new Patient("Vilde", "G", "Heidi","13515");
        patientRegister.addPatient(p1);
        assertFalse(patientRegister.addPatient(p1));
    }

    @Test
    void removePatientTrue() {
        patientRegister = new PatientRegister();
        Patient p1 = new Patient("Vilde", "G", "Heidi","13515");
        patientRegister.addPatient(p1);
        Patient p2 = new Patient("Ida", "G", "Heidi","252343");
        patientRegister.addPatient(p2);

        assertTrue(patientRegister.removePatient(p2));
    }
    @Test
    void removePatientFalse() {
        patientRegister = new PatientRegister();
        Patient p1 = new Patient("Vilde", "G", "Heidi","13515");
        patientRegister.addPatient(p1);
        Patient p2 = new Patient("Ida", "G", "Heidi","252343");
        //p2 not added

        assertFalse(patientRegister.removePatient(p2));
    }
}