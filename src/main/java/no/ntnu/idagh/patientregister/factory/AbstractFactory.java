package no.ntnu.idagh.patientregister.factory;

public interface AbstractFactory<T> {
    T create(String type);
}

