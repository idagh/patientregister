package no.ntnu.idagh.patientregister.factory.menu;

import no.ntnu.idagh.patientregister.factory.menu.Menu;

public class HelpMenu implements Menu {
    @Override
    public String getMenu(){
        return "helpMenu";
    }
}

