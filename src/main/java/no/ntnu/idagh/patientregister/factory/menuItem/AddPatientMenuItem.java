package no.ntnu.idagh.patientregister.factory.menuItem;

public class AddPatientMenuItem  implements MenuItem {
    @Override
    public String getMenuItem() {
        return "addPatient";
    }
}

