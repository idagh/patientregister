package no.ntnu.idagh.patientregister.factory.menuItem;

import no.ntnu.idagh.patientregister.factory.AbstractFactory;

public class MenuItemFactory implements AbstractFactory<MenuItem> {
    @Override
    public MenuItem create(String type) {
        if ("about".equalsIgnoreCase(type)) {
        } else if ("addPatient".equalsIgnoreCase(type)) {
            return new AddPatientMenuItem();
        } else if ("editPatient".equalsIgnoreCase(type)) {
            return new EditPatientMenuItem();
        } else if ("exit".equalsIgnoreCase(type)) {
            return new ExitMenuItem();
        }
        return null;
    }
}

