package no.ntnu.idagh.patientregister.factory.menuItem;

public class EditPatientMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "editPatient";
    }
}

