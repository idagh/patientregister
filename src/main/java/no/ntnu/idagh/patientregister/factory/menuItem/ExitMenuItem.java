package no.ntnu.idagh.patientregister.factory.menuItem;

public class ExitMenuItem implements MenuItem{
    @Override
    public String getMenuItem() {
        return "exit";
    }
}
