package no.ntnu.idagh.patientregister.factory.menuItem;

public interface MenuItem {
    String getMenuItem();
}

