package no.ntnu.idagh.patientregister.factory;

import no.ntnu.idagh.patientregister.factory.menu.MenuFactory;
import no.ntnu.idagh.patientregister.factory.menuItem.MenuItemFactory;

public class FactoryProvider {
    public static AbstractFactory<?> getFactory(String choice) {
        if ("ImageView".equalsIgnoreCase(choice)) {
        } else if ("Menu".equalsIgnoreCase(choice)) {
            return new MenuFactory();
        } else if ("MenuItem".equalsIgnoreCase(choice)) {
            return new MenuItemFactory();
        }
        return null;
    }
}

