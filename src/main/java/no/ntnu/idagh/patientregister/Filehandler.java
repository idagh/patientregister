package no.ntnu.idagh.patientregister;

import java.io.*;
import java.nio.file.Files;

/**
 *  A class that represents a file handler.
 *  @version 04.05.2021
 *  @author idagh
 */
public class Filehandler {
    private static final String DELIMITER = ";";

    /**
     * left empty and private to avoid making an
     * instance of a file handler. Every method
     * is static and is possible to access from other
     * classes.
     */
    private Filehandler() {

    }

    /**
     * exports a patient register to a file
     * @param patientRegister
     * @param file
     * @throws IOException
     */

    public static void exportToFile(PatientRegister patientRegister, File file) throws IOException {
        PrintWriter printWriter = new PrintWriter(new FileWriter(file));
        boolean title = true;
        for (Patient p : patientRegister.getPatients()) {
            if (title) {
                printWriter.println("socialSecurityNumber"+ DELIMITER +"firstName" + DELIMITER + "lastName" + DELIMITER + "generalPractitioner" + DELIMITER + "diagnosis");
                title = false;
            } else {
                printWriter.println(p.getSocialSecurityNumber() + DELIMITER + p.getFirstName() + DELIMITER + p.getLastName() + DELIMITER + p.getGeneralPractitioner() + DELIMITER + p.getDiagnosis());
            }
        }
        printWriter.close();
    }


    /**
     * imports a file to a patient register
     * @param patientRegister
     * @param file
     * @throws IOException
     */
    public static void importFromFile(PatientRegister patientRegister, File file) throws IOException {
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath())){
            String line = bufferedReader.readLine();
            while(line != null) {
                Patient patient = getLineAsPatientObject(line);
                if (!patient.getFirstName().equalsIgnoreCase("firstname")) {
                    patientRegister.addPatient(patient);
                }
                line = bufferedReader.readLine();
            }
        }
    }

    /**
     * saves the patientregister to a file
     * @param patientRegister
     * @param file
     * @throws IOException
     */
    public static void saveToFile(PatientRegister patientRegister, File file) throws IOException  {
        try (OutputStream outputStream = Files.newOutputStream(file.toPath())) {
            ObjectOutputStream objectOutStream = new ObjectOutputStream(outputStream);
            objectOutStream.writeObject(patientRegister);
        }

    }


    /**
     * gets a line from file to a patient object
     * @param line
     * @return patient object from line
     * @throws IOException
     */
    public static Patient getLineAsPatientObject(String line) throws IOException {
        String[] values = line.split(DELIMITER);
        Patient patient;
        if (values.length==5) { //to read different files
            patient = new Patient(values[0], values[1], values[2], values[3], values[4]);
            return patient;
        } else if (values.length==4) {
            patient = new Patient(values[0], values[1], values[2], values[3]);
            return patient;
        } else if (values.length==3) {
            patient = new Patient(values[0], values[1], values[2]);
            return patient;
        } else {
            return null;
        }
    }
}
