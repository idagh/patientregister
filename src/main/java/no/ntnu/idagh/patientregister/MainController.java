package no.ntnu.idagh.patientregister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 *  A class that represents a controller.
 *  @version 04.05.2021
 *  @author idagh
 */
public class MainController {
    private PatientRegister patientRegister;
    private ObservableList<Patient> observableList;
    private Button okBtn;
    private TextField firstNameInput;
    private TextField lastNameInput;
    private TextField ssnInput;
    private File currentFile;


    @FXML
    public TableView<Patient> tableView;
    public Button addBtn;
    public Button editBtn;
    public TableColumn<Patient, String> firstNameColumn;
    public TableColumn<Patient, String> lastNameColumn;
    public TableColumn<Patient, String> socialSecurityNumberColumn;

    /**
     * initializes the application, creates an
     * instance of patientregister and an observable
     * list to show in tableview
     */
    public void initialize() {
        this.patientRegister = new PatientRegister();
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        patientRegister.addPatient(new Patient("ida", "heg", "", "942724"));
        this.observableList = FXCollections.observableArrayList();

        fillTable();
    }

    /**
     * creates a stage for adding or editing a patient
     * @return stage
     */
    public Stage addEditPatient() {
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setMinWidth(250);
        stage.setMinHeight(150);
        stage.setResizable(false);

        Label firstNameLabel = new Label("First name:");
        Label lastNameLabel = new Label("Last name:");
        Label ssnLabel = new Label("Social security number:");

        VBox labels = new VBox();
        labels.getChildren().addAll(firstNameLabel, lastNameLabel, ssnLabel);
        labels.setSpacing(10);
        firstNameLabel.setPadding(new Insets(5, 0, 5, 0));
        lastNameLabel.setPadding(new Insets(5, 0, 5, 0));
        ssnLabel.setPadding(new Insets(5, 0, 5, 0));

        firstNameInput = new TextField();
        firstNameInput.setPromptText("First name");
        lastNameInput = new TextField();
        lastNameInput.setPromptText("Last name");
        ssnInput = new TextField();
        ssnInput.setPromptText("Social security number");

        VBox inputFields = new VBox();
        inputFields.getChildren().addAll(firstNameInput, lastNameInput, ssnInput);
        inputFields.setSpacing(10);

        HBox inputs = new HBox();
        inputs.getChildren().addAll(labels, inputFields);
        inputs.setSpacing(10);

        okBtn = new Button("OK");
        Button cancelBtn = new Button("Cancel");
        cancelBtn.setDefaultButton(true);

        HBox buttons = new HBox();
        buttons.getChildren().addAll(okBtn, cancelBtn);
        buttons.setSpacing(10);
        buttons.setAlignment(Pos.BOTTOM_RIGHT);

        cancelBtn.setOnAction(c -> stage.close());

        VBox layout = new VBox();
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.setSpacing(20);

        layout.getChildren().addAll(inputs, buttons);
        stage.setScene(new Scene(layout));
        return stage;
    }

    /**
     * method to add patient from GUI
     * calls addEditPatient method to get stage
     * checks if the input is valid.
     * creates a patient object if input is valid
     * and adds to patientRegister
     */
    public void addPatient() {
        Stage stage = addEditPatient();
        stage.setTitle("Patient details - Add");

        okBtn.setOnAction(c -> {
            String firstName = firstNameInput.getText();
            String lastName = lastNameInput.getText();
            String ssn = ssnInput.getText();

            if (firstName.equals("") || lastName.equals("") || ssn.equals("")) {
                showInformationDialog(
                        "Message Dialog - Add",
                        "Invalid input",
                        "Input fields cannot be empty"
                );
            } else {
                Patient patient = new Patient(ssn, firstName, lastName, "", "");
                patientRegister.addPatient(patient);
                fillTable();

                stage.close();
            }
        });

        stage.showAndWait();
    }

    /**
     * method to edit patient from GUI
     * calls addEditPatient method to get stage
     * checks if a patient is selected
     * sets different values to the patient
     */
    public void editPatient() {
        Patient patient = tableView.getSelectionModel().getSelectedItem();

        if (patient == null) {
            showInformationDialog(
                    "Message Dialog - Edit",
                    "No selected item",
                    "You have to select an item to edit"
            );
        } else {
            Stage stage = addEditPatient();
            stage.setTitle("Patient details - Edit");

            firstNameInput.setText(patient.getFirstName());
            lastNameInput.setText(patient.getLastName());
            ssnInput.setText(patient.getSocialSecurityNumber());

            okBtn.setOnAction(c -> {
                patient.setFirstName(firstNameInput.getText());
                patient.setLastName(lastNameInput.getText());
                patient.setSocialSecurityNumber(ssnInput.getText());
                fillTable();
                stage.close();
            });

            stage.showAndWait();
        }
    }

    /**
     * method to delete patient from GUI
     * checks if a patient is selected
     * a confirmation window shows to confirm
     * delete. Deletes patient
     *
     * @throws IOException
     */
    public void deletePatient() throws IOException {
        Patient patient = tableView.getSelectionModel().getSelectedItem();

        if (patient == null) {
            showInformationDialog(
                    "Message Dialog - Edit",
                    "No selected item",
                    "You have to select an item to edit"
            );
        } else {
            Boolean confirm = showConfirmDialog(
                    "Delete confirmation",
                    "Delete confirmation",
                    "Are you sure you want to delete this item?\n" + patient.getLastName() + ", " + patient.getFirstName()
            );

            if (confirm) {
                patientRegister.removePatient(patient);
                fillTable();
            }
        }
    }

    /**
     * method to show about info
     * shows a message dialog
     */
    public void showAboutInfo() {
        showInformationDialog(
                "Information Dialog - About",
                "Patients register\nv1.0-SNAPSHOT",
                "An application created by\nIda Green Heglund\n2021-04-20"
        );
    }

    /**
     * method to import file
     * opens a file chooser, and user can
     * select only .csv files.
     */
    public void importFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        File file = fileChooser.showOpenDialog(null);
        currentFile = file;
        try {
            Filehandler.importFromFile(this.patientRegister, file);
            fillTable();
        } catch (IOException e) {
            showInformationDialog("Warning", "Error in importing file","Could not import file, try again");
        }

    }

    /**
     * method to export file
     * opens a file chooser, and user can
     * save file.
     */
    public void exportFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        File file = fileChooser.showSaveDialog(null);
        currentFile = file;
        try {
            Filehandler.exportToFile(this.patientRegister, file);
            fillTable();
        } catch (IOException e) {
            showInformationDialog("Warning", "Error in exporting file","Could not import file, try again");
        }
    }

    /**
     * method to fill tableview.
     * updates the observable list
     */
    public void fillTable() {
        tableView.getItems().clear();
        ArrayList<Patient> patients = patientRegister.getPatients();
        this.observableList.setAll(patients);
        tableView.setItems(this.observableList);
    }

    /**
     * method to show information dialog
     *
     * @param title the title of message
     * @param header the heade of the message
     * @param message the message to the user
     */
    public void showInformationDialog(String title, String header, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }
    }

    public boolean showConfirmDialog(String title, String header, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public void exitApplication(){
        Boolean confirm = showConfirmDialog(
                "Confirm message",
                "Confirm system exit",
                "Are you sure you want to exit the system?"
        );
        if (confirm) {
            System.exit(0);
        }
    }
}
