package no.ntnu.idagh.patientregister;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent main = FXMLLoader.load(getClass().getResource("scenes/main.fxml"));
        Scene scene = new Scene(main);
        stage.setScene(scene);
        stage.show();
    }
}
