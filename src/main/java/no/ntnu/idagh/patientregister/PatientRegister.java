package no.ntnu.idagh.patientregister;

import java.util.ArrayList;

/**
 *  A class that represents a patient register.
 *  @version 04.05.2021
 *  @author idagh
 */
public class PatientRegister {
    private final ArrayList<Patient> patients;

    public PatientRegister() {
        this.patients = new ArrayList<>();
    }

    public ArrayList<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Adds a Patient to the PatientRegister
     *
     * @param patient
     * @return true if patient is not already in register, false if not
     */
    public boolean addPatient(Patient patient) {
        boolean add = true;
        for (Patient p : patients) {
            if (patient.equals(p)) {
                add = false;
            }
        }
        if (add) {
            patients.add(patient);
        }
        return add;
    }

    /**
     * removes a patient from patientregister
     * @param patient
     * @return true if successful, false if not
     */
    public boolean removePatient(Patient patient) {
        for (Patient p : this.patients) {
            if (p == patient) {
                this.patients.remove(patient);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "PatientRegister{" +
                "patients=" + patients +
                '}';
    }
}
